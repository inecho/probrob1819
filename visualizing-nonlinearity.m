ux = linspace(0,1,500);
theta = linspace(0,1,500);
x = linspace(0,1,500);
%w = normrnd(0,0.0001,500,1); %mu,sigma,rows,cols
w = exp(-2*linspace(0,1.5,500)'+sin(linspace(0,1.8,500)));
ucos = ux*cos(theta+w)';
y = x + ucos;
plot(y)