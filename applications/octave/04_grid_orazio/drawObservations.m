function drawObservations(map_, observations_, row_, col_, orientation_)
  map_rows = rows(map_);
	hold on;
  
  global HEADING_UP;
  global HEADING_RIGHT;
  global HEADING_DOWN;
  global HEADING_LEFT;
	
	#check the observation array
	if (observations_(1)) #FRONT
    if (orientation_ == HEADING_UP) 
      rectangle("Position", [col_-1 map_rows-row_+1 1 0.5], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_DOWN)
      rectangle("Position", [col_-1 map_rows-row_-0.5 1 0.5], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_RIGHT) 
      rectangle("Position", [col_ map_rows-row_ 0.5 1], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_LEFT) 
          rectangle("Position", [col_-1.5 map_rows-row_ 0.5 1], "FaceColor", "blue", "EdgeColor", "none");
    endif
	endif
  
	if (observations_(2)) #REAR
    if (orientation_ == HEADING_UP) 
      rectangle("Position", [col_-1 map_rows-row_-0.5 1 0.5], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_DOWN)
       rectangle("Position", [col_-1 map_rows-row_+1 1 0.5], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_RIGHT) 
      rectangle("Position", [col_-1.5 map_rows-row_ 0.5 1], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_LEFT) 
      rectangle("Position", [col_ map_rows-row_ 0.5 1], "FaceColor", "blue", "EdgeColor", "none");
    endif
	endif
  
	if (observations_(3)) #LEFT
    if (orientation_ == HEADING_UP) 
      rectangle("Position", [col_-1.5 map_rows-row_ 0.5 1], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_DOWN)
      rectangle("Position", [col_ map_rows-row_ 0.5 1], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_RIGHT) 
      rectangle("Position", [col_-1 map_rows-row_+1 1 0.5], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_LEFT) 
      rectangle("Position", [col_-1 map_rows-row_-0.5 1 0.5], "FaceColor", "blue", "EdgeColor", "none");
    endif
	endif
  
	if (observations_(4)) #RIGHT
    if (orientation_ == HEADING_UP) 
      rectangle("Position", [col_ map_rows-row_ 0.5 1], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_DOWN)
      rectangle("Position", [col_-1.5 map_rows-row_ 0.5 1], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_RIGHT) 
      rectangle("Position", [col_-1 map_rows-row_-0.5 1 0.5], "FaceColor", "blue", "EdgeColor", "none");
    endif
    if (orientation_ == HEADING_LEFT) 
      rectangle("Position", [col_-1 map_rows-row_+1 1 0.5], "FaceColor", "blue", "EdgeColor", "none");
    endif
	endif
  
	
	hold off;
endfunction

