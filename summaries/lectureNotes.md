# Events and Probability
- The statement "The Signal strength falls into the interval $S_{i}$" describes an event.

  $$ P_{l_{j}}(S = s_{i}) = P_{l_{j}}(S_{i})  $$

- The Probability is a function going from each measurable subset of the event space $\Omega$
  to the interval $[0,1]$
> testddddddddd


## Axioms
  |$$ P(E) \geq 0 $$| The probability of an event is greater or equal than 0|
  |---|---|
  |**$$P(\cup_{i}E_{i} = \sum_{i} P(E_{i}) )$$** | **The probability of the union of a set of disjoint events is the sum of probabilities** |
  |**$$P(\Omega) = 1$$** | **The probability of the outcome, being in the set of possible outcomes is 1**|

---
# Continuous Probability
-
-
-
-
-
-


$$ P(X < x) $$

$$ P(x) = \frac{\partial P(x)}{\partial x} $$



$$ P(A|B) = \frac{P(B|A)P(B)}{P(A)} $$
 It's just $$ \int_{a \in X}^{\infty} omething$$ Else
