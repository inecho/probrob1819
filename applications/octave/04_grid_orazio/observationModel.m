function observation_probability = observationModel(map_, row_, col_, orientation_, observations_)
  observation_probability = 1;

  #evaluate cell occupancy
  cell_up_occupied    = 0;
  cell_down_occupied  = 0;
  cell_left_occupied  = 0;
  cell_right_occupied = 0;
  
  global HEADING_UP;
  global HEADING_DOWN;
  global HEADING_LEFT;
  global HEADING_RIGHT;
  
  # OBSERVING IN FORWARD DIRECTION
  if (row_-1 > 0 && orientation_ == HEADING_UP) # if the row above is a valid row
	  cell_up_occupied    = map_(row_-1, col_); # either the cell is 1 or 0, occupied or not
  elseif (row_+1 <= rows(map_) && orientation_ == HEADING_DOWN)
    cell_up_occupied = map_(row_+1, col_);
  elseif (col_+1 <= columns(map_) && orientation_ == HEADING_RIGHT)
    cell_up_occupied = map_(row_, col_+1);
  elseif (col_-1 > 0 && orientation_ == HEADING_LEFT)
    cell_up_occupied = map_(row_, col_-1);
  endif
  
  # OBSERVING IN BACKWARD DIRECTION
  if (row_+1 <= rows(map_) && orientation_ == HEADING_UP)
    cell_down_occupied  = map_(row_+1, col_);
  elseif (row_-1 > 0 && orientation_ == HEADING_DOWN)
    cell_down_occupied  = map_(row_-1, col_);    
  elseif (col_-1 > 0 && orientation_ == HEADING_RIGHT)
    cell_down_occupied  = map_(row_, col_-1);
  elseif (col_+1 <= columns(map_) && orientation_ == HEADING_LEFT)
    cell_down_occupied  = map_(row_, col_+1);
  endif
  
  # OBSERVING IN RIGHT DIRECTION
  if (col_+1 <= columns(map_) && orientation_ == HEADING_UP)
	  cell_right_occupied = map_(row_, col_+1);

  elseif (col_-1 > 0 && orientation_ == HEADING_DOWN)
    cell_right_occupied = map_(row_, col_-1);
    
  elseif (row_+1 <= rows(map_) && orientation_ == HEADING_RIGHT)
    cell_right_occupied = map_(row_+1, col_);
    
  elseif (row_-1 > 0 && orientation_ == HEADING_LEFT)
    cell_right_occupied = map_(row_-1, col_);
  endif
  
  # OBSERVING IN LEFT DIRECTION
  if (col_-1 > 0 && orientation_ == HEADING_UP)
	  cell_left_occupied  = map_(row_, col_-1);
  elseif (col_+1 <= columns(map_) && orientation_ == HEADING_DOWN)
    cell_left_occupied  = map_(row_, col_+1);
  elseif ( row_-1 > 0 && orientation_ == HEADING_RIGHT)
    cell_left_occupied  = map_(row_-1, col_);
  elseif ( row_+1 <= rows(map_) && orientation_ == HEADING_LEFT)
    cell_left_occupied  = map_(row_+1, col_);
  endif
  
%  cell_up_occupied
%  cell_down_occupied
%  cell_left_occupied
%  cell_right_occupied
  #update probability depending on observations
  if (cell_up_occupied == observations_(1))
	  observation_probability *= 0.8; #true positive
  else
	  observation_probability *= 0.2; #false positive
  endif	    
  if (cell_down_occupied == observations_(2))
	  observation_probability *= 0.8; #true positive
  else
	  observation_probability *= 0.2; #false positive
  endif
  if (cell_left_occupied == observations_(3))
	  observation_probability *= 0.8; #true positive
  else
	  observation_probability *= 0.2; #false positive
  endif	
  if (cell_right_occupied == observations_(4))
	  observation_probability *= 0.8; #true positive
  else
	  observation_probability *= 0.2; #false positive
  endif	
endfunction

