source "../tools/utilities/geometry_helpers_3d.m"

function [e,J]=errorAndJacobian(x,p,z)
  rx=Rx(x(4));
  ry=Ry(x(5));
  rz=Rz(x(6));
  t=x(1:3);
  
  z_hat=rx*ry*rz*p+t;
  e=z_hat-z;
  J=zeros(3,6);
  J(1:3,1:3)=eye(3);

  rx_prime=Rx_prime(x(4));
  ry_prime=Ry_prime(x(5));
  rz_prime=Rz_prime(x(6));
  
  J(1:3,4)=rx_prime*ry*rz*p;
  J(1:3,5)=rx*ry_prime*rz*p;
  J(1:3,6)=rx*ry*rz_prime*p;
endfunction

function [x, chi_stats, num_inliers]=doICP(x_guess, P, Z, num_iterations, damping, kernel_threshold, plotting)
  x=x_guess;
  chi_stats=zeros(1,num_iterations); #ignore this for now
  num_inliers=zeros(1,num_iterations);
  for (iteration=1:num_iterations)
    H=zeros(6,6);
    b=zeros(6,1);
    chi_stats(iteration)=0;
    for (i=1:size(P,2))
      [e,J] = errorAndJacobian(x, P(:,i), Z(:,i));
      chi=e'*e;
      if (chi>kernel_threshold)
	      e*=sqrt(kernel_threshold/chi);
	      chi=kernel_threshold;
      else
	      num_inliers(iteration)++;
      endif;
      chi;
      chi_stats(iteration)+=chi;
      H+=J'*J;
      b+=J'*e;
    endfor
    H+=eye(6)*damping;
    dx=-H\b;
    x+=dx;

    if(plotting)
      % plotting each iteration
      P_hom = [P; ones(size(P)(2), 1)'];
      X_guess = v2t(x_guess);
      Z_hom = [Z; ones(size(Z)(2), 1)'];
      Z_guess = X_guess*Z_hom;

      hold off
      plot3(P(1, :), P(2, :), P(3, :), "ro")
      hold on
      % plot3(Z_guess(1, :), Z_guess(2, :), Z_guess(3, :), "b.")
      plot3(Z(1, :), Z(2, :), Z(3, :), "b.")
      axis equal

      X_result = v2t(x);
      X_i = X_result * P_hom;
      plot3(X_i(1,:), X_i(2,:), X_i(3,:), "g.");
      line([X_i(1,:); Z(1, :)], 
          [X_i(2,:); Z(2, :)], 
          [X_i(3,:); Z(3, :)])

      pause(0.000001)
    endif

  endfor
endfunction


function [e,J]=errorAndJacobianManifold(X,p,z)
% X is a homogenous transformation Maxtrix [R|t]
 z_hat=X(1:3,1:3)*p+X(1:3,4); #prediction
 e=z_hat-z;
 J=zeros(3,6);
 J(1:3,1:3)=eye(3);
 J(1:3,4:6)=-skew(z_hat);
%  pause
endfunction

function [X, chi_stats]=doICPManifold(X_guess, P, Z, n_it,damping, threshold, plotting)
 X=v2t(X_guess); % we optimize the transformation matrix directly
 chi_stats=zeros(1,n_it);
 for (iteration=1:n_it)
  H=zeros(6,6);
  b=zeros(6,1);
  chi=0;
  for (i=1:size(P,2))
    [e,J] = errorAndJacobianManifold(X, P(:,i), Z(:,i));
    chi+=e'*e;
    if(chi>threshold)
      e*=sqrt(threshold/chi);
      chi = threshold;
    endif;
    H+=J'*J;
    b+=J'*e;
    chi_stats(iteration)+=chi;
  endfor
  % e
  chi
  H+=eye(6)*damping;
  dx=-H\b;
  X=v2t(dx)*X; % boxplus

  if(plotting)
    % plotting each iteration
    P_hom = [P; ones(size(P)(2), 1)'];
    % X_guess = v2t(x_guess);
    % Z_hom = [Z; ones(size(Z)(2), 1)'];
    % Z_guess = v2t(X_guess)*Z_hom;

    hold off
    plot3(P(1, :), P(2, :), P(3, :), "ro")
    hold on
    % plot3(Z_guess(1, :), Z_guess(2, :), Z_guess(3, :), "b.")
    plot3(Z(1, :), Z(2, :), Z(3, :), "b.")
    axis equal

    X_i = X * P_hom;
    plot3(X_i(1,:), X_i(2,:), X_i(3,:), "g.");
    line([X_i(1,:); Z(1, :)], 
        [X_i(2,:); Z(2, :)], 
        [X_i(3,:); Z(3, :)])

    pause(1)
  endif

 endfor
endfunction