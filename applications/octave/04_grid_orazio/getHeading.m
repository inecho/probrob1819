function current_heading = getHeading(control_input_, state_ground_truth_)
  global MOVE_FORWARD;
  global MOVE_BACKWARD;
  global ROTATE_LEFT;
  global ROTATE_RIGHT;

  orientation_ = state_ground_truth_(3);

  switch (control_input_)
    case ROTATE_RIGHT
      if(orientation_ < 4) # do not exceed range 1-4
        orientation_++;
      else
        orientation_ = 1;
      endif
    case ROTATE_LEFT
      if (orientation_ > 1)
        orientation_--;
      else
        orientation_ = 4;
      endif
  endswitch
  current_heading = orientation_;
endfunction