function orientationString = orientation2string(orientation_)
  if (orientation_ == 1)
    orientationString = "HEADING_UP";
  elseif (orientation_ == 2)
    orientationString = "HEADING_RIGHT";
  elseif (orientation_ == 3)
    orientationString = "HEADING_DOWN";
  elseif (orientation_ == 4)
    orientationString = "HEADING_LEFT";
  endif
endfunction