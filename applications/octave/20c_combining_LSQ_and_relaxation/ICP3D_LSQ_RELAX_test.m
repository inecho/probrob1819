pkg load geometry

source "../tools/utilities/geometry_helpers_3d.m"
source "./icp_3d_LSQ.m"

plotting = true;
if(plotting)
    h = figure(1);
    axis equal
endif

n_points=50;
P_world=(rand(3,n_points)-0.5)*10;

#ideal position of world w.r.t robot
x_true=[0, 0, 0, pi/2, pi/6, pi]';
% x_true=[0.5, 0.5, 0.5, 0, 0, 0]';
X_true=v2t(x_true);

#compute the measurements by mapping them in the observer frame 
P_world_hom = ones(4, n_points);
P_world_hom(1:3, :) = P_world;
X_true;
Z_hom = X_true*P_world_hom;
Z = Z_hom(1:3,:);

iterations=100;
damping=10; # damping factor

chi_stats=zeros(2,iterations);
inliers_stats=zeros(2,iterations);

# test with a good initial guess
x_guess=x_true+[2 ,2 , 2, 1.5, 1.9, 1.7]'; % the chi shows a gonvergance, but it's stuck at a local minima
% x_guess = [0.5, 0.5, 0.5, 0.1, 0.1, 0.1]' ;% after a certain iteration it's locked in the angles
% x_guess=[0.25, 0.25, 0.25, 0, 0, 0]';
X_guess = v2t(x_guess) * P_world_hom;

% hold on
% plot3(P_world(1,:), P_world(2,:), P_world(3,:), 'b.')
% plot3(X_guess(1,:), X_guess(2,:), X_guess(3,:), 'k.')
% plot3(Z(1,:), Z(2,:), Z(3,:), "r.")

% [x_result, chi_stats(1,:),  inliers_stats(1,:)] = doICP(x_guess, P_world, Z, iterations, damping, 1e9, plotting);
[X, chi_stats]=doICPManifold(x_guess, P_world, Z, iterations, damping, 1e9, plotting);

% X_res = X*P_world_hom;
% X_res = X_res(1:3,:);
% plot3(X_res(1,:), X_res(2,:), X_res(3,:), 'ko')

if(plotting)
    waitfor(h)
end

X_result = v2t(x_result) * P_world_hom;

distances = sqrt(sum(X_result(1:3,:) - P_world).^2);
dist_median = median(distances)
dist_std = std(distances)


% icp with linear relaxation
[R_est_rlx, t_est_rlx] = icp(X_result(1:3,:), P_world);

angles = RotMat2Euler(R_est_rlx)


X_result_rlx = R_est_rlx*P_world+t_est_rlx;


distances_rlx = sqrt(sum(X_result_rlx - P_world(1:3,:)).^2);

dist_median_rlx = median(distances_rlx)
dist_std_rlx = std(distances_rlx)