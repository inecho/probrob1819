#import 2d geometry utils
source "../../tools/utilities/geometry_helpers_2d.m"
source "../exercise/ls_calibrate_odometry.m"

input = load("/home/rud/preps/probabilistic_robotics_2018_19/applications/octave/datasets/differential_drive_calibration_data/differential_drive_calibration.txt");

cutoff = 1
cutoff = floor(length(input)/cutoff);
input = input(1:cutoff, :);

tl = input(:,1);
tr = input(:,2);
x =input(:,3);
y =input(:,4);
theta =input(:,5);

kr = 1;
kl = -1;
b = 0.3;

r = kr*tr;
l = kl*tl;

dtheta = (r-l)./b;

taylor_sin = 1 - ((dtheta.^2) ./ 6) + ((dtheta.^4) ./ 120) - ((dtheta.^6) ./ 5040);
taylor_cos = (dtheta ./ 2) - ((dtheta.^3) ./ 24) + ((dtheta.^5) ./ 720);


dx = ((r+l) / 2) .* taylor_sin;
dy = ((r+l) / 2) .* taylor_cos;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T1 = compute_odometry_trajectory([dx, dy, dtheta]);
T2 = compute_odometry_trajectory([x, y, theta]);

z_hx = ([x y theta dx dy dtheta]);


X=ls_calibrate_odometry(z_hx); disp(X)

function X = ls_calibrate_odometry(z_hx)
	#accumulator variables for the linear system
	H=zeros(9, 9);
	b=zeros(9, 1);
	#initial solution (the identity transformation)
	X=eye(3); 
	
	#loop through the measurements and update the
	#accumulators
	for i=1:size(z_hx,1),
		e=error_function(i,X,z_hx);
		A=jacobian(i,Z);
		H = H + A' * A;
		b = b + A' * e;
	end
    
	#solve the linear system
	deltaX=-H\b;
	#this reshapes the 9x1 increment vector in a 3x3 atrix
	dX=reshape(deltaX,3,3)';
	#computes the cumulative solution
	X=X+dX;
end


hx = z_hx(:, 4:6);
corrected_odom=apply_odometry_correction(X, hx);

T3=compute_odometry_trajectory(corrected_odom);

h = figure();
hold on;
plot(T1(:,1), T1(:,2), 'r-', 'linewidth', 1);
plot(T2(:,1), T2(:,2), 'g-', 'linewidth', 1);
plot(T3(:,1), T3(:,2), 'b', 'linewidth', 2);
hold off;
waitfor(h)
