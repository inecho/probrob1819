function h = plotArrow(map_, row_, col_, orientation_)
  pkg load geometry
  global HEADING_UP;
  global HEADING_DOWN;
  global HEADING_LEFT;
  global HEADING_RIGHT;
  c = col_;
  r = rows(map_)-row_;
    if(orientation_ == HEADING_UP )
      c = c-0.5;
      h = drawArrow ([c,r,c,r+1], 1,1,1,0.5);
    elseif(orientation_ == HEADING_RIGHT)
      r = r+0.5;
      h = drawArrow ([c,r,c,r], 1,1,1,0.5);
    elseif(orientation_ == HEADING_DOWN)
      c = c-0.5;
      r = r+1;
      h = drawArrow ([c,r,c,r-1], 1,1,1,0.5);
    elseif(orientation_ == HEADING_LEFT)
      r = r+0.5;
      h = drawArrow ([c,r,c-1,r], 1,1,1,0.5);
    endif
  set (h.body, "color", "w")
  set (h.head, "edgecolor", "w")
  set (h.head, "facecolor", "g")
 hold off;
endfunction