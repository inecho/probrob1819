#import 2d geometry utils
source "../../tools/utilities/geometry_helpers_2d.m"
source "../exercise/ls_calibrate_odometry.m"

<<<<<<< HEAD
input = load("/home/mango/probabilistic_robotics_2018_19/applications/octave/datasets/differential_drive_calibration_data/differential_drive_calibration.txt");
=======
input = load("/home/rud/preps/probabilistic_robotics_2018_19/applications/octave/datasets/differential_drive_calibration_data/differential_drive_calibration.txt");

cutoff = 1;
cutoff = floor(length(input)/cutoff);
input = input(1:cutoff, :);
>>>>>>> 90a3f5494918348df3274622e3dd5ce44880152e

tl = input(:,1);
tr = input(:,2);
x =input(:,3);
y =input(:,4);
<<<<<<< HEAD
theta =input(:,5)
=======
theta =input(:,5);
>>>>>>> 90a3f5494918348df3274622e3dd5ce44880152e

kr = 1;
kl = -1;
b = 0.3;

r = kr*tr;
l = kl*tl;

dtheta = (r-l)./b;

taylor_sin = 1 - ((dtheta.^2) ./ 6) + ((dtheta.^4) ./ 120) - ((dtheta.^6) ./ 5040);
<<<<<<< HEAD
taylor_cos = (dtheta / 2) - ((dtheta.^3) ./ 24) + ((dtheta.^5) ./ 720);
dx = ((r+l) ./ 2) .* taylor_sin
dy = ((r+l) ./ 2) .* taylor_cos;

t1 = compute_odometry_trajectory([dx dy dtheta]);


h = figure();
plot(t1(:,1), t1(:,2), 'r', 'linewidth', 2);

=======
taylor_cos = (dtheta ./ 2) - ((dtheta.^3) ./ 24) + ((dtheta.^5) ./ 720);


dx = ((r+l) / 2) .* taylor_sin;
dy = ((r+l) / 2) .* taylor_cos;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

z_hx = ([x y theta dx dy dtheta]);

X = ls_calibrate_odometry(z_hx); disp(X)

hx = z_hx(:, 4:6);
corrected_odom = apply_odometry_correction(X, hx);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T1 = compute_odometry_trajectory([dx, dy, dtheta]);
T2 = compute_odometry_trajectory([x, y, theta]);
T3 = compute_odometry_trajectory(corrected_odom);

h = figure();
hold on;
plot(T1(:,1), T1(:,2), 'r-', 'linewidth', 1);
plot(T2(:,1), T2(:,2), 'g-', 'linewidth', 1);
plot(T3(:,1), T3(:,2), 'b', 'linewidth', 2);
hold off;
>>>>>>> 90a3f5494918348df3274622e3dd5ce44880152e
waitfor(h)
