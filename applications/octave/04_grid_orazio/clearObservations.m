function clearObservations(observations_, row_, col_, map_)
	map_rows = rows(map_);
	hold on;
  
  global HEADING_UP;
  global HEADING_RIGHT;
  global HEADING_DOWN;
  global HEADING_LEFT;
	
  #check the observation array
	if (observations_(1)) # FRONT SENSOR
	  if (map_(row_-1, col_) && HEADING_UP) # POINTS UP
	    rectangle("Position", [col_-1 map_rows-row_+1 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-1 map_rows-row_+1 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_, col_+1) && HEADING_RIGHT) # POINTS RIGHT
	    rectangle("Position", [col_ map_rows-row_ 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_ map_rows-row_ 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_+1, col_) && HEADING_DOWN) # POINTS DOWN
	    rectangle("Position", [col_-1 map_rows-row_-1 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-1 map_rows-row_-1 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_, col_-1) && HEADING_LEFT) # POINTS LEFT
	    rectangle("Position", [col_-2 map_rows-row_ 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-2 map_rows-row_ 1 1], "FaceColor", "white");
    endif
	endif
  
	if (observations_(3)) #REAR SENSOR
	  if (map_(row_+1, col_) && HEADING_UP) # POINTS DOWN
	    rectangle("Position", [col_-1 map_rows-row_-1 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-1 map_rows-row_-1 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_, col_-1) && HEADING_RIGHT) # POINTS LEFT
	    rectangle("Position", [col_-2 map_rows-row_ 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-2 map_rows-row_ 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_-1, col_) && HEADING_DOWN)  # POINTS UP
	    rectangle("Position", [col_-1 map_rows-row_+1 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-1 map_rows-row_+1 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_, col_+1) && HEADING_LEFT) # POINTS RIGHT
	    rectangle("Position", [col_ map_rows-row_ 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_ map_rows-row_ 1 1], "FaceColor", "white");
    endif
    
	endif
	
  if (observations_(4)) #LEFT SENSOR
	  if (map_(row_, col_-1) && HEADING_UP) # POINTS LEFT
	    rectangle("Position", [col_-2 map_rows-row_ 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-2 map_rows-row_ 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_-1, col_) && HEADING_RIGHT) # POINTS UP
	    rectangle("Position", [col_-1 map_rows-row_+1 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-1 map_rows-row_+1 1 1], "FaceColor", "white");
    endif
    
 	  if (map_(row_, col_+1) && HEADING_DOWN) # POINTS RIGHT
	    rectangle("Position", [col_ map_rows-row_ 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_ map_rows-row_ 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_+1, col_) && HEADING_LEFT) # POINTS DOWN
	    rectangle("Position", [col_-1 map_rows-row_-1 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-1 map_rows-row_-1 1 1], "FaceColor", "white");
    endif  
  
	endif
	
  if (observations_(2)) #RIGHT
	  if (map_(row_, col_+1) && HEADING_UP) # POINTS RIGHT
	    rectangle("Position", [col_ map_rows-row_ 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_ map_rows-row_ 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_+1, col_) && HEADING_RIGHT) # POINTS DOWN
	    rectangle("Position", [col_-1 map_rows-row_-1 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-1 map_rows-row_-1 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_, col_-1) && HEADING_DOWN) # POINTS LEFT
	    rectangle("Position", [col_-2 map_rows-row_ 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-2 map_rows-row_ 1 1], "FaceColor", "white");
    endif
    
    if (map_(row_-1, col_) && HEADING_LEFT) # POINTS UP
	    rectangle("Position", [col_-1 map_rows-row_+1 1 1], "FaceColor", "black");
	  else
      rectangle("Position", [col_-1 map_rows-row_+1 1 1], "FaceColor", "white");
    endif
    
	endif
	
	hold off;
endfunction

